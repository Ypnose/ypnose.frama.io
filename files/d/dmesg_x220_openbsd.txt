OpenBSD 5.7-current (RAMDISK_CD) #896: Fri May 15 17:17:41 MDT 2015
    deraadt@amd64.openbsd.org:/usr/src/sys/arch/amd64/compile/RAMDISK_CD
real mem = 6303641600 (6011MB)
avail mem = 6110879744 (5827MB)
mainbus0 at root
bios0 at mainbus0: SMBIOS rev. 2.6 @ 0xdae9c000 (64 entries)
bios0: vendor LENOVO version "8DET69WW (1.39 )" date 07/18/2013
bios0: LENOVO 4291M41
acpi0 at bios0: rev 2
acpi0: tables DSDT FACP SLIC SSDT SSDT SSDT SSDT HPET APIC MCFG ECDT ASF! TCPA SSDT SSDT UEFI UEFI UEFI
acpimadt0 at acpi0 addr 0xfee00000: PC-AT compat
cpu0 at mainbus0: apid 0 (boot processor)
cpu0: Intel(R) Core(TM) i5-2520M CPU @ 2.50GHz, 797.54 MHz
cpu0: FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,CFLUSH,DS,ACPI,MMX,FXSR,SSE,SSE2,SS,HTT,TM,PBE,SSE3,PCLMUL,DTES64,MWAIT,DS-CPL,VMX,SMX,EST,TM2,SSSE3,CX16,xTPR,PDCM,PCID,SSE4.1,SSE4.2,x2APIC,POPCNT,DEADLINE,AES,XSAVE,AVX,NXE,LONG,LAHF,PERF,ITSC
cpu0: 256KB 64b/line 8-way L2 cache
cpu0: apic clock running at 99MHz
cpu at mainbus0: not configured
cpu at mainbus0: not configured
cpu at mainbus0: not configured
ioapic0 at mainbus0: apid 2 pa 0xfec00000, version 20, 24 pins
acpiec0 at acpi0
acpiprt0 at acpi0: bus 0 (PCI0)
acpiprt1 at acpi0: bus -1 (PEG_)
acpiprt2 at acpi0: bus 2 (EXP1)
acpiprt3 at acpi0: bus 3 (EXP2)
acpiprt4 at acpi0: bus -1 (EXP4)
acpiprt5 at acpi0: bus 13 (EXP5)
acpiprt6 at acpi0: bus -1 (EXP7)
pci0 at mainbus0 bus 0
pchb0 at pci0 dev 0 function 0 "Intel Core 2G Host" rev 0x09
vga1 at pci0 dev 2 function 0 "Intel HD Graphics 3000" rev 0x09
wsdisplay0 at vga1 mux 1: console (80x25, vt100 emulation)
"Intel 6 Series MEI" rev 0x04 at pci0 dev 22 function 0 not configured
"Intel 6 Series KT" rev 0x04 at pci0 dev 22 function 3 not configured
em0 at pci0 dev 25 function 0 "Intel 82579LM" rev 0x04: msi, address f0:de:f1:6e:5b:8a
ehci0 at pci0 dev 26 function 0 "Intel 6 Series USB" rev 0x04: apic 2 int 16
usb0 at ehci0: USB revision 2.0
uhub0 at usb0 "Intel EHCI root hub" rev 2.00/1.00 addr 1
"Intel 6 Series HD Audio" rev 0x04 at pci0 dev 27 function 0 not configured
ppb0 at pci0 dev 28 function 0 "Intel 6 Series PCIE" rev 0xb4: msi
pci1 at ppb0 bus 2
ppb1 at pci0 dev 28 function 1 "Intel 6 Series PCIE" rev 0xb4: msi
pci2 at ppb1 bus 3
iwn0 at pci2 dev 0 function 0 "Intel Centrino Advanced-N 6205" rev 0x34: msi, MIMO 2T2R, MoW, address a0:88:b4:74:92:40
ppb2 at pci0 dev 28 function 4 "Intel 6 Series PCIE" rev 0xb4: msi
pci3 at ppb2 bus 13
sdhc0 at pci3 dev 0 function 0 "Ricoh 5U823 SD/MMC" rev 0x04: apic 2 int 16
sdmmc0 at sdhc0
ehci1 at pci0 dev 29 function 0 "Intel 6 Series USB" rev 0x04: apic 2 int 23
usb1 at ehci1: USB revision 2.0
uhub1 at usb1 "Intel EHCI root hub" rev 2.00/1.00 addr 1
"Intel QM67 LPC" rev 0x04 at pci0 dev 31 function 0 not configured
pciide0 at pci0 dev 31 function 2 "Intel 6 Series SATA" rev 0x04: DMA, channel 0 configured to native-PCI, channel 1 configured to native-PCI
pciide0: using apic 2 int 19 for native-PCI interrupt
"Intel 6 Series SMBus" rev 0x04 at pci0 dev 31 function 3 not configured
pciide1 at pci0 dev 31 function 5 "Intel 6 Series SATA" rev 0x04: DMA, channel 0 wired to native-PCI, channel 1 wired to native-PCI
pciide1: using apic 2 int 19 for native-PCI interrupt
isa0 at mainbus0
pckbc0 at isa0 port 0x60/5
pckbd0 at pckbc0 (kbd slot): using irq 1
wskbd0 at pckbd0: console keyboard, using wsdisplay0
: using irq 12uhub2 at uhub0 port 1 "vendor 0x8087 product 0x0024" rev 2.00/0.00 addr 2
umass0 at uhub2 port 2 configuration 1 interface 0 "SMI Corporation USB DISK" rev 2.00/11.00 addr 3
umass0: using SCSI over Bulk-Only
scsibus0 at umass0: 2 targets, initiator 0
sd0 at scsibus0 targ 1 lun 0: <, , 1100> SCSI0 0/direct removable serial.090c1000000360007317
sd0: 1947MB, 512 bytes/sector, 3987456 sectors
uhub3 at uhub1 port 1 "vendor 0x8087 product 0x0024" rev 2.00/0.00 addr 2
softraid0 at root
scsibus1 at softraid0: 256 targets
root on rd0a swap on rd0b dump on rd0b
