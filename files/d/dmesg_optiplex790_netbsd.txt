Copyright (c) 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
    2006, 2007, 2008, 2009, 2010, 2011, 2012
    The NetBSD Foundation, Inc.  All rights reserved.
Copyright (c) 1982, 1986, 1989, 1991, 1993
    The Regents of the University of California.  All rights reserved.

NetBSD 6.1.5 (GENERIC)
total memory = 4053 MB
avail memory = 3920 MB
timecounter: Timecounters tick every 10.000 msec
RTC BIOS diagnostic error 0x3f<config_unit,memory_size,fixed_disk,invalid_time>
timecounter: Timecounter "i8254" frequency 1193182 Hz quality 100
Dell Inc. OptiPlex 790 (01)
mainbus0 (root)
ACPI Warning: 32/64X FACS address mismatch in FADT - 0xCF7E4E40/0x00000000CF7E4D40, using 32 (20110623/tbfadt-517)
cpu0 at mainbus0 apid 0: Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz, id 0x206a7
cpu1 at mainbus0 apid 2: Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz, id 0x206a7
cpu2 at mainbus0 apid 4: Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz, id 0x206a7
cpu3 at mainbus0 apid 6: Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz, id 0x206a7
ioapic0 at mainbus0 apid 2: pa 0xfec00000, version 20, 24 pins
acpi0 at mainbus0: Intel ACPICA 20110623
acpi0: X/RSDT: OemId <DELL  , CBX3   ,06222004>, AslId <MSFT,00010013>
acpi0: SCI interrupting at int 9
timecounter: Timecounter "ACPI-Fast" frequency 3579545 Hz quality 1000
hpet0 at acpi0: high precision event timer (mem 0xfed00000-0xfed00400)
timecounter: Timecounter "hpet0" frequency 14318180 Hz quality 2000
FWHD (INT0800) at acpi0 not configured
LDRC (PNP0C02) at acpi0 not configured
attimer1 at acpi0 (TIMR, PNP0100): io 0x40-0x43,0x50-0x53 irq 0
CWDT (INT3F0D) at acpi0 not configured
COM1 (PNP0501) at acpi0 not configured
PDRC (PNP0C02) at acpi0 not configured
MEM2 (PNP0C01) at acpi0 not configured
acpibut0 at acpi0 (PWRB, PNP0C0C-170): ACPI Power Button
pci0 at mainbus0 bus 0: configuration mode 1
pci0: i/o space, memory space enabled, rd/line, rd/mult, wr/inv ok
pchb0 at pci0 dev 0 function 0: vendor 0x8086 product 0x0100 (rev. 0x09)
ppb0 at pci0 dev 1 function 0: vendor 0x8086 product 0x0101 (rev. 0x09)
ppb0: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci1 at ppb0 bus 1
pci1: i/o space, memory space enabled, rd/line, wr/inv ok
vga0 at pci1 dev 0 function 0: vendor 0x1002 product 0x68f9 (rev. 0x00)
wsdisplay0 at vga0 kbdmux 1: console (80x25, vt100 emulation)
wsmux1: connecting to wsdisplay0
radeondrm0 at vga0: ATI Radeon HD 5450
radeondrm0: Initialized radeon 1.29.0 20080613
vendor 0x8086 product 0x1c3a (miscellaneous communications, revision 0x04) at pci0 dev 22 function 0 not configured
wm0 at pci0 dev 25 function 0: PCH2 LAN (82579LM) Controller (rev. 0x04)
wm0: interrupting at ioapic0 pin 20
wm0: PCI-Express bus
wm0: FLASH
wm0: Ethernet address 18:03:73:47:e5:c9
ihphy0 at wm0 phy 2: i82579 10/100/1000 media interface, rev. 3
ihphy0: 10baseT, 10baseT-FDX, 100baseTX, 100baseTX-FDX, 1000baseT, 1000baseT-FDX, auto
ehci0 at pci0 dev 26 function 0: vendor 0x8086 product 0x1c2d (rev. 0x04)
ehci0: interrupting at ioapic0 pin 16
ehci0: EHCI version 1.0
usb0 at ehci0: USB revision 2.0
hdaudio0 at pci0 dev 27 function 0: HD Audio Controller
hdaudio0: interrupting at ioapic0 pin 22
hdafg0 at hdaudio0: Realtek ALC269
hdafg0: DAC00 2ch: Speaker [Built-In]
hdafg0: DAC01 2ch: Speaker [Jack], HP Out [Jack]
hdafg0: ADC02 2ch: Mic In [Jack]
hdafg0: ADC03 2ch: Mic In [Jack]
hdafg0: 2ch/2ch 44100Hz 48000Hz 96000Hz 192000Hz PCM16 PCM20 PCM24 AC3
audio0 at hdafg0: full duplex, playback, capture, independent
ppb1 at pci0 dev 28 function 0: vendor 0x8086 product 0x1c10 (rev. 0xb4)
ppb1: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci2 at ppb1 bus 2
pci2: i/o space, memory space enabled, rd/line, wr/inv ok
ppb2 at pci0 dev 28 function 2: vendor 0x8086 product 0x1c14 (rev. 0xb4)
ppb2: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci3 at ppb2 bus 3
pci3: i/o space, memory space enabled, rd/line, wr/inv ok
ehci1 at pci0 dev 29 function 0: vendor 0x8086 product 0x1c26 (rev. 0x04)
ehci1: interrupting at ioapic0 pin 17
ehci1: EHCI version 1.0
usb1 at ehci1: USB revision 2.0
ppb3 at pci0 dev 30 function 0: vendor 0x8086 product 0x244e (rev. 0xa4)
pci4 at ppb3 bus 4
pci4: i/o space, memory space enabled
ichlpcib0 at pci0 dev 31 function 0: vendor 0x8086 product 0x1c4c (rev. 0x04)
timecounter: Timecounter "ichlpcib0" frequency 3579545 Hz quality 1000
ichlpcib0: 24-bit timer
ichlpcib0: TCO (watchdog) timer configured.
ahcisata0 at pci0 dev 31 function 2: vendor 0x8086 product 0x1c02 (rev. 0x04)
ahcisata0: interrupting at ioapic0 pin 18
ahcisata0: 64-bit DMA
ahcisata0: AHCI revision 1.30, 6 ports, 32 slots, CAP 0xe730ff45<EMS,PSC,SSC,PMD,ISS=0x3=Gen3,SCLO,SAL,SALP,SSNTF,SNCQ,S64A>
atabus0 at ahcisata0 channel 0
atabus1 at ahcisata0 channel 1
ichsmb0 at pci0 dev 31 function 3: vendor 0x8086 product 0x1c22 (rev. 0x04)
ichsmb0: interrupting at ioapic0 pin 18
iic0 at ichsmb0: I2C bus
isa0 at ichlpcib0
lpt0 at isa0 port 0x378-0x37b irq 7
tpm0 at isa0 iomem 0xfed40000-0xfed44fff irq 7: device 0x0000104a rev 0x4e
com0 at isa0 port 0x3f8-0x3ff irq 4: ns16550a, working fifo
pckbc0 at isa0 port 0x60-0x64
pckbd0 at pckbc0 (kbd slot)
pckbc0: using irq 1 for kbd slot
wskbd0 at pckbd0: console keyboard, using wsdisplay0
pcppi0 at isa0 port 0x61
midi0 at pcppi0: PC speaker
sysbeep0 at pcppi0
attimer1: attached to pcppi0
acpicpu0 at cpu0: ACPI CPU
acpicpu0: C1: FFH, lat   1 us, pow  1000 mW
acpicpu0: C2: FFH, lat  80 us, pow   500 mW
acpicpu0: C3: FFH, lat 104 us, pow   350 mW
acpicpu0: P0: FFH, lat  10 us, pow 95000 mW, 3301 MHz, turbo boost
acpicpu0: P1: FFH, lat  10 us, pow 95000 mW, 3300 MHz
acpicpu0: P2: FFH, lat  10 us, pow 82600 mW, 3000 MHz
acpicpu0: P3: FFH, lat  10 us, pow 75316 mW, 2800 MHz
acpicpu0: P4: FFH, lat  10 us, pow 68319 mW, 2600 MHz
acpicpu0: P5: FFH, lat  10 us, pow 61575 mW, 2400 MHz
acpicpu0: P6: FFH, lat  10 us, pow 55107 mW, 2200 MHz
acpicpu0: P7: FFH, lat  10 us, pow 48886 mW, 2000 MHz
acpicpu0: P8: FFH, lat  10 us, pow 42921 mW, 1800 MHz
acpicpu0: P9: FFH, lat  10 us, pow 37215 mW, 1600 MHz
acpicpu0: T0: I/O, lat   1 us, pow     0 mW, 100 %
acpicpu0: T1: I/O, lat   1 us, pow     0 mW,  88 %
acpicpu0: T2: I/O, lat   1 us, pow     0 mW,  76 %
acpicpu0: T3: I/O, lat   1 us, pow     0 mW,  64 %
acpicpu0: T4: I/O, lat   1 us, pow     0 mW,  52 %
acpicpu0: T5: I/O, lat   1 us, pow     0 mW,  40 %
acpicpu0: T6: I/O, lat   1 us, pow     0 mW,  28 %
acpicpu0: T7: I/O, lat   1 us, pow     0 mW,  16 %
coretemp0 at cpu0: thermal sensor, 1 C resolution
acpicpu1 at cpu1: ACPI CPU
coretemp1 at cpu1: thermal sensor, 1 C resolution
acpicpu2 at cpu2: ACPI CPU
coretemp2 at cpu2: thermal sensor, 1 C resolution
acpicpu3 at cpu3: ACPI CPU
coretemp3 at cpu3: thermal sensor, 1 C resolution
timecounter: Timecounter "clockinterrupt" frequency 100 Hz quality 0
timecounter: Timecounter "TSC" frequency 3292626200 Hz quality 3000
uhub0 at usb0: vendor 0x8086 EHCI root hub, class 9/0, rev 2.00/1.00, addr 1
uhub0: 2 ports with 2 removable, self powered
uhub1 at usb1: vendor 0x8086 EHCI root hub, class 9/0, rev 2.00/1.00, addr 1
uhub1: 2 ports with 2 removable, self powered
ahcisata0 port 0: device present, speed: 6.0Gb/s
ahcisata0 port 1: device present, speed: 1.5Gb/s
wd0 at atabus0 drive 0
wd0: <WDC WD2500AAKX-753CA1>
wd0: drive supports 16-sector PIO transfers, LBA48 addressing
wd0: 232 GB, 484521 cyl, 16 head, 63 sec, 512 bytes/sect x 488397168 sectors
wd0: drive supports PIO mode 4, DMA mode 2, Ultra-DMA mode 6 (Ultra/133)
wd0(ahcisata0:0:0): using PIO mode 4, DMA mode 2, Ultra-DMA mode 6 (Ultra/133) (using DMA)
atapibus0 at atabus1: 1 targets
cd0 at atapibus0 drive 0: <HL-DT-ST DVD+-RW GT60N, KZWC1EN4441, A101> cdrom removable
cd0: drive supports PIO mode 4, DMA mode 2, Ultra-DMA mode 6 (Ultra/133)
cd0(ahcisata0:1:0): using PIO mode 4, DMA mode 2, Ultra-DMA mode 6 (Ultra/133) (using DMA)
uhub2 at uhub0 port 1: vendor 0x8087 product 0x0024, class 9/0, rev 2.00/0.00, addr 2
uhub2: single transaction translator
uhub3 at uhub1 port 1: vendor 0x8087 product 0x0024, class 9/0, rev 2.00/0.00, addr 2
uhub3: single transaction translator
uhub2: 6 ports with 6 removable, self powered
uhub3: 8 ports with 8 removable, self powered
uhidev0 at uhub3 port 7 configuration 1 interface 0
uhidev0: Dell Dell USB Entry Keyboard, rev 1.10/1.15, addr 3, iclass 3/1
ukbd0 at uhidev0
wskbd1 at ukbd0 mux 1
wskbd1: connecting to wsdisplay0
Kernelized RAIDframe activated
pad0: outputs: 44100Hz, 16-bit, stereo
audio1 at pad0: half duplex, playback, capture
boot device: cd0
root on cd0a dumps on cd0b
root file system type: cd9660
