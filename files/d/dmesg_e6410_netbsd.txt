Copyright (c) 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005,
    2006, 2007, 2008, 2009, 2010, 2011, 2012
    The NetBSD Foundation, Inc.  All rights reserved.
Copyright (c) 1982, 1986, 1989, 1991, 1993
    The Regents of the University of California.  All rights reserved.

NetBSD 6.1.5 (GENERIC)
total memory = 3953 MB
avail memory = 3824 MB
timecounter: Timecounters tick every 10.000 msec
RTC BIOS diagnostic error 0x30<config_unit,memory_size>
timecounter: Timecounter "i8254" frequency 1193182 Hz quality 100
Dell Inc. Latitude E6410 (0001)
mainbus0 (root)
ACPI Warning: 32/64X FACS address mismatch in FADT - 0xCF36BF40/0x00000000CF36ED40, using 32 (20110623/tbfadt-517)
cpu0 at mainbus0 apid 0: Intel(R) Core(TM) i7 CPU       M 620  @ 2.67GHz, id 0x20655
cpu1 at mainbus0 apid 4: Intel(R) Core(TM) i7 CPU       M 620  @ 2.67GHz, id 0x20655
cpu2 at mainbus0 apid 1: Intel(R) Core(TM) i7 CPU       M 620  @ 2.67GHz, id 0x20655
cpu3 at mainbus0 apid 5: Intel(R) Core(TM) i7 CPU       M 620  @ 2.67GHz, id 0x20655
ioapic0 at mainbus0 apid 2: pa 0xfec00000, version 20, 24 pins
acpi0 at mainbus0: Intel ACPICA 20110623
acpi0: X/RSDT: OemId <DELL  , E2     ,06222004>, AslId <MSFT,00010013>
acpi0: SCI interrupting at int 9
timecounter: Timecounter "ACPI-Fast" frequency 3579545 Hz quality 1000
hpet0 at acpi0: high precision event timer (mem 0xfed00000-0xfed00400)
timecounter: Timecounter "hpet0" frequency 14318180 Hz quality 2000
acpiec0 at acpi0 (ECDV, PNP0C09-0): io 0x930,0x934
acpivga0 at acpi0 (VID): ACPI Display Adapter
acpiout0 at acpivga0 (CRT, 0x80000100): ACPI Display Output Device
acpiout1 at acpivga0 (LCD, 0x80002400): ACPI Display Output Device
acpiout1: brightness levels: 0 6 13 20 26 33 40 46 53 60 66 73 80 86 93 100
acpiout2 at acpivga0 (DVI, 0x80007302): ACPI Display Output Device
acpiout3 at acpivga0 (DVI2, 0x80007303): ACPI Display Output Device
acpiout4 at acpivga0 (DP, 0x80006300): ACPI Display Output Device
acpiout5 at acpivga0 (DP2, 0x80006301): ACPI Display Output Device
acpivga0: connected output devices:
acpivga0:   0x0100 (acpiout0): VGA Analog Monitor, index 0, port 0, head 0
acpivga0:   0x2400 (acpiout1): Int. Digital Flat Panel, index 0, port 0, head 0
acpivga0:   0x7302 (acpiout2): Ext. Digital Monitor, index 2, port 0, head 0
acpivga0:   0x7303 (acpiout3): Ext. Digital Monitor, index 3, port 0, head 0
acpivga0:   0x6300 (acpiout4): Ext. Digital Monitor, index 0, port 0, head 0
acpivga0:   0x6301 (acpiout5): Ext. Digital Monitor, index 1, port 0, head 0
FWHD (INT0800) at acpi0 not configured
LDRC (PNP0C02) at acpi0 not configured
attimer1 at acpi0 (TIMR, PNP0100): io 0x40-0x43,0x50-0x53 irq 0
pckbc1 at acpi0 (PS2K, PNP0303) (kbd port): io 0x60,0x64 irq 1
pckbc2 at acpi0 (PS2M, DLL040A) (aux port): irq 12
PDRC (PNP0C02) at acpi0 not configured
A_CC (SMO8800) at acpi0 not configured
acpilid0 at acpi0 (LID, PNP0C0D): ACPI Lid Switch
acpibut0 at acpi0 (PBTN, PNP0C0C): ACPI Power Button
acpibut1 at acpi0 (SBTN, PNP0C0E): ACPI Sleep Button
acpiacad0 at acpi0 (AC, ACPI0003): ACPI AC Adapter
acpibat0 at acpi0 (BAT0, PNP0C0A-1): ACPI Battery
acpibat0: Samsung SDI LION rechargeable battery
acpibat0: granularity: low->warn 0.056 Ah, warn->full 0.056 Ah
acpibat1 at acpi0 (BAT1, PNP0C0A-2): ACPI Battery
acpiwmi0 at acpi0 (AMW0, PNP0C14-0): ACPI WMI Interface
wmidell0 at acpiwmi0: Dell WMI mappings
pckbd0 at pckbc1 (kbd slot)
pckbc1: using irq 1 for kbd slot
wskbd0 at pckbd0: console keyboard
pms0 at pckbc1 (aux slot)
pckbc1: using irq 12 for aux slot
wsmouse0 at pms0 mux 0
pci0 at mainbus0 bus 0: configuration mode 1
pci0: i/o space, memory space enabled, rd/line, rd/mult, wr/inv ok
pchb0 at pci0 dev 0 function 0: vendor 0x8086 product 0x0044 (rev. 0x02)
agp0 at pchb0: can't find internal VGA device config space
ppb0 at pci0 dev 1 function 0: vendor 0x8086 product 0x0045 (rev. 0x02)
ppb0: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci1 at ppb0 bus 1
pci1: i/o space, memory space enabled, rd/line, wr/inv ok
vga0 at pci1 dev 0 function 0: vendor 0x10de product 0x0a6c (rev. 0xa2)
wsdisplay0 at vga0 kbdmux 1: console (80x25, vt100 emulation), using wskbd0
wsmux1: connecting to wsdisplay0
drm at vga0 not configured
hdaudio0 at pci1 dev 0 function 1: HD Audio Controller
hdaudio0: interrupting at ioapic0 pin 17
hdafg0 at hdaudio0: NVIDIA GT21x HDMI
hdafg0: DP00 8ch: Digital Out [Jack]
hdafg0: 8ch/0ch 48000Hz PCM16*
hdafg1 at hdaudio0: NVIDIA GT21x HDMI
hdafg1: DP00 8ch: Digital Out [Jack]
hdafg1: 8ch/0ch 48000Hz PCM16*
hdafg2 at hdaudio0: NVIDIA GT21x HDMI
hdafg2: DP00 8ch: Digital Out [Jack]
hdafg2: 8ch/0ch 48000Hz PCM16*
hdafg3 at hdaudio0: NVIDIA GT21x HDMI
hdafg3: DP00 8ch: Digital Out [Jack]
hdafg3: 8ch/0ch 48000Hz PCM16*
vendor 0x8086 product 0x3b64 (miscellaneous communications, revision 0x06) at pci0 dev 22 function 0 not configured
puc0 at pci0 dev 22 function 3: Intel 5 Series KT (com)
com2 at puc0 port 0: interrupting at ioapic0 pin 19
com2: ns16550a, working fifo
wm0 at pci0 dev 25 function 0: PCH LAN (82577LM) Controller (rev. 0x05)
wm0: interrupting at ioapic0 pin 20
wm0: PCI-Express bus
wm0: FLASH
wm0: Ethernet address xx:xx:xx:xx:xx:xx
ihphy0 at wm0 phy 2: i82577 10/100/1000 media interface, rev. 3
ihphy0: 10baseT, 10baseT-FDX, 100baseTX, 100baseTX-FDX, 1000baseT, 1000baseT-FDX, auto
ehci0 at pci0 dev 26 function 0: vendor 0x8086 product 0x3b3c (rev. 0x05)
ehci0: interrupting at ioapic0 pin 16
ehci0: EHCI version 1.0
usb0 at ehci0: USB revision 2.0
hdaudio1 at pci0 dev 27 function 0: HD Audio Controller
hdaudio1: interrupting at ioapic0 pin 22
hdafg4 at hdaudio1: Sigmatel 92HD81B1C5
hdafg4: DAC00 2ch: Speaker [Built-In], HP Out [Jack]
hdafg4: ADC01 2ch: Mic In [Jack]
hdafg4: ADC02 2ch: Mic In [Built-In]
hdafg4: DAC03 2ch: Speaker [Jack]
hdafg4: 2ch/2ch 44100Hz 48000Hz 88200Hz 96000Hz 192000Hz PCM16 PCM20 PCM24 AC3
audio0 at hdafg4: full duplex, playback, capture, independent
ppb1 at pci0 dev 28 function 0: vendor 0x8086 product 0x3b42 (rev. 0x05)
ppb1: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci2 at ppb1 bus 2
pci2: i/o space, memory space enabled, rd/line, wr/inv ok
ppb2 at pci0 dev 28 function 1: vendor 0x8086 product 0x3b44 (rev. 0x05)
ppb2: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci3 at ppb2 bus 3
pci3: i/o space, memory space enabled, rd/line, wr/inv ok
iwn0 at pci3 dev 0 function 0: vendor 0x8086 product 0x422c (rev. 0x35)
iwn0: interrupting at ioapic0 pin 17
iwn0: MIMO 2T2R, MoW, address xx:xx:xx:xx:xx:xx
iwn0: 11a rates: 6Mbps 9Mbps 12Mbps 18Mbps 24Mbps 36Mbps 48Mbps 54Mbps
iwn0: 11b rates: 1Mbps 2Mbps 5.5Mbps 11Mbps
iwn0: 11g rates: 1Mbps 2Mbps 5.5Mbps 11Mbps 6Mbps 9Mbps 12Mbps 18Mbps 24Mbps 36Mbps 48Mbps 54Mbps
ppb3 at pci0 dev 28 function 2: vendor 0x8086 product 0x3b46 (rev. 0x05)
ppb3: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci4 at ppb3 bus 4
pci4: i/o space, memory space enabled, rd/line, wr/inv ok
sdhc0 at pci4 dev 0 function 0: vendor 0x1180 product 0xe822 (rev. 0x03)
sdhc0: interrupting at ioapic0 pin 19
sdhc0: SD Host Specification 3.0, rev.4
sdmmc0 at sdhc0
fwohci0 at pci4 dev 0 function 4: vendor 0x1180 product 0xe832 (rev. 0x03)
fwohci0: interrupting at ioapic0 pin 16
fwohci0: OHCI version 1.0 (ROM=0)
fwohci0: No. of Isochronous channels is 4.
fwohci0: EUI64 xx:xx:xx:xx:xx:xx:c0:00
fwohci0: Phy 1394a available S400, 1 ports.
fwohci0: Link S400, max_rec 2048 bytes.
ieee1394if0 at fwohci0: IEEE1394 bus
fwip0 at ieee1394if0: IP over IEEE1394
fwohci0: Initiate bus reset
ppb4 at pci0 dev 28 function 3: vendor 0x8086 product 0x3b48 (rev. 0x05)
ppb4: PCI Express 2.0 <Root Port of PCI-E Root Complex>
pci5 at ppb4 bus 6
pci5: i/o space, memory space enabled, rd/line, wr/inv ok
ehci1 at pci0 dev 29 function 0: vendor 0x8086 product 0x3b34 (rev. 0x05)
ehci1: interrupting at ioapic0 pin 17
ehci1: EHCI version 1.0
usb1 at ehci1: USB revision 2.0
ppb5 at pci0 dev 30 function 0: vendor 0x8086 product 0x2448 (rev. 0xa5)
pci6 at ppb5 bus 12
pci6: i/o space, memory space enabled
pcib0 at pci0 dev 31 function 0: vendor 0x8086 product 0x3b07 (rev. 0x05)
ahcisata0 at pci0 dev 31 function 2: vendor 0x8086 product 0x282a (rev. 0x05)
ahcisata0: interrupting at ioapic0 pin 19
ahcisata0: 64-bit DMA
ahcisata0: AHCI revision 1.30, 6 ports, 32 slots, CAP 0xe720ff65<SXS,EMS,PSC,SSC,PMD,ISS=0x2=Gen2,SCLO,SAL,SALP,SSNTF,SNCQ,S64A>
atabus0 at ahcisata0 channel 0
atabus1 at ahcisata0 channel 1
atabus2 at ahcisata0 channel 4
atabus3 at ahcisata0 channel 5
ichsmb0 at pci0 dev 31 function 3: vendor 0x8086 product 0x3b30 (rev. 0x05)
ichsmb0: interrupting at ioapic0 pin 18
iic0 at ichsmb0: I2C bus
vendor 0x8086 product 0x3b32 (miscellaneous DASP, revision 0x05) at pci0 dev 31 function 6 not configured
isa0 at pcib0
tpm0 at isa0 iomem 0xfed40000-0xfed44fff irq 7: device 0x200114e4 rev 0x20
pcppi0 at isa0 port 0x61
midi0 at pcppi0: PC speaker
sysbeep0 at pcppi0
attimer1: attached to pcppi0
pci7 at mainbus0 bus 63
pci7: i/o space, memory space enabled, rd/line, rd/mult, wr/inv ok
pchb1 at pci7 dev 0 function 0: vendor 0x8086 product 0x2c62 (rev. 0x02)
pchb2 at pci7 dev 0 function 1: vendor 0x8086 product 0x2d01 (rev. 0x02)
pchb3 at pci7 dev 2 function 0: vendor 0x8086 product 0x2d10 (rev. 0x02)
pchb4 at pci7 dev 2 function 1: vendor 0x8086 product 0x2d11 (rev. 0x02)
pchb5 at pci7 dev 2 function 2: vendor 0x8086 product 0x2d12 (rev. 0x02)
pchb6 at pci7 dev 2 function 3: vendor 0x8086 product 0x2d13 (rev. 0x02)
acpicpu0 at cpu0: ACPI CPU
acpicpu0: C1: FFH, lat   3 us, pow  1000 mW
acpicpu0: C2: FFH, lat 205 us, pow   500 mW
acpicpu0: C3: FFH, lat 245 us, pow   350 mW, bus master check
acpicpu0: P0: FFH, lat  10 us, pow 25000 mW, 2667 MHz, turbo boost
acpicpu0: P1: FFH, lat  10 us, pow 25000 mW, 2666 MHz
acpicpu0: P2: FFH, lat  10 us, pow 23465 mW, 2533 MHz
acpicpu0: P3: FFH, lat  10 us, pow 21982 mW, 2399 MHz
acpicpu0: P4: FFH, lat  10 us, pow 20527 mW, 2266 MHz
acpicpu0: P5: FFH, lat  10 us, pow 19080 mW, 2133 MHz
acpicpu0: P6: FFH, lat  10 us, pow 17681 mW, 1999 MHz
acpicpu0: P7: FFH, lat  10 us, pow 16310 mW, 1866 MHz
acpicpu0: P8: FFH, lat  10 us, pow 14966 mW, 1733 MHz
acpicpu0: P9: FFH, lat  10 us, pow 13665 mW, 1599 MHz
acpicpu0: P10: FFH, lat  10 us, pow 12375 mW, 1466 MHz
acpicpu0: P11: FFH, lat  10 us, pow 11112 mW, 1333 MHz
acpicpu0: P12: FFH, lat  10 us, pow  9877 mW, 1199 MHz
acpicpu0: T0: I/O, lat   1 us, pow     0 mW, 100 %
acpicpu0: T1: I/O, lat   1 us, pow     0 mW,  88 %
acpicpu0: T2: I/O, lat   1 us, pow     0 mW,  76 %
acpicpu0: T3: I/O, lat   1 us, pow     0 mW,  64 %
acpicpu0: T4: I/O, lat   1 us, pow     0 mW,  52 %
acpicpu0: T5: I/O, lat   1 us, pow     0 mW,  40 %
acpicpu0: T6: I/O, lat   1 us, pow     0 mW,  28 %
acpicpu0: T7: I/O, lat   1 us, pow     0 mW,  16 %
coretemp0 at cpu0: thermal sensor, 1 C resolution
acpicpu1 at cpu1: ACPI CPU
coretemp1 at cpu1: thermal sensor, 1 C resolution
acpicpu2 at cpu2: ACPI CPU
acpicpu3 at cpu3: ACPI CPU
fwohci0: BUS reset
fwohci0: node_id=0xc800ffc0, gen=1, CYCLEMASTER mode
ieee1394if0: 1 nodes, maxhop <= 0 cable IRM irm(0) (me)
ieee1394if0: bus manager 0
timecounter: Timecounter "clockinterrupt" frequency 100 Hz quality 0
acpiacad0: AC adapter online.
uhub0 at usb0: vendor 0x8086 EHCI root hub, class 9/0, rev 2.00/1.00, addr 1
uhub0: 3 ports with 3 removable, self powered
uhub1 at usb1: vendor 0x8086 EHCI root hub, class 9/0, rev 2.00/1.00, addr 1
uhub1: 3 ports with 3 removable, self powered
ahcisata0 port 0: device present, speed: 1.5Gb/s
ahcisata0 port 1: device present, speed: 1.5Gb/s
wd0 at atabus0 drive 0
wd0: <ST910021AS>
wd0: drive supports 16-sector PIO transfers, LBA48 addressing
wd0: 95396 MB, 193821 cyl, 16 head, 63 sec, 512 bytes/sect x 195371568 sectors
wd0: drive supports PIO mode 4, DMA mode 2, Ultra-DMA mode 6 (Ultra/133)
wd0(ahcisata0:0:0): using PIO mode 4, DMA mode 2, Ultra-DMA mode 6 (Ultra/133) (using DMA)
atapibus0 at atabus1: 1 targets
cd0 at atapibus0 drive 0: <TSSTcorp DVD+/-RW TS-U633F, R3476GSZ706995, D500> cdrom removable
cd0: drive supports PIO mode 4, DMA mode 2, Ultra-DMA mode 5 (Ultra/100)
cd0(ahcisata0:1:0): using PIO mode 4, DMA mode 2, Ultra-DMA mode 5 (Ultra/100) (using DMA)
uhub2 at uhub1 port 1: vendor 0x8087 product 0x0020, class 9/0, rev 2.00/0.00, addr 2
uhub2: single transaction translator
uhub3 at uhub0 port 1: vendor 0x8087 product 0x0020, class 9/0, rev 2.00/0.00, addr 2
uhub3: single transaction translator
uhub2: 8 ports with 8 removable, self powered
uhub3: 6 ports with 6 removable, self powered
umass0 at uhub2 port 3 configuration 1 interface 0
umass0: SMI Corporation USB DISK, rev 2.00/11.00, addr 3
umass0: using SCSI over Bulk-Only
scsibus0 at umass0: 2 targets, 1 lun per target
sd0 at scsibus0 target 0 lun 0: <, , 1100> disk removable
uvideo0 at uhub3 port 4 configuration 1 interface 0: CN06YWTK7248707F09FZ Laptop_Integrated_Webcam_3M, rev 2.00/3.31, addr 3
video0 at uvideo0: CN06YWTK7248707F09FZ Laptop_Integrated_Webcam_3M, rev 2.00/3.31, addr 3
sd0: fabricating a geometry
sd0: 1947 MB, 1947 cyl, 64 head, 32 sec, 512 bytes/sect x 3987456 sectors
sd0: fabricating a geometry
ubt0 at uhub2 port 7
ubt0: Dell Computer Corp DW375 Bluetooth Module, rev 2.00/5.17, addr 4
ugen0 at uhub2 port 8
ugen0: Broadcom Corp 5880, rev 1.10/1.01, addr 5
Kernelized RAIDframe activated
pad0: outputs: 44100Hz, 16-bit, stereo
audio1 at pad0: half duplex, playback, capture
boot device: sd0
root on sd0a dumps on sd0b
root file system type: ffs
WARNING: clock gained 179 days
acpibat0: normal capacity on 'charge state'
